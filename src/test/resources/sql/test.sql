DROP TABLE IF EXISTS user;
 
CREATE TABLE user (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  first_name VARCHAR(250) NOT NULL,
  last_name VARCHAR(250) NOT NULL,
  birth_date DATE DEFAULT NULL,
  email VARCHAR(250) DEFAULT NULL,
  address VARCHAR(500) DEFAULT NULL
);

INSERT INTO user (first_name, last_name, birth_date, email, address)
VALUES ('Lokesh', 'Gupta', '2019-10-10', 'howtodoinjava@gmail.com', 'address 1'),
       ('John', 'Doe', '2017-1-25', 'xyz@email.com', 'address 2');