package com.example.user.integration;

import com.example.user.UserApplication;
import io.restassured.RestAssured;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.h2.tools.RunScript;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.context.WebApplicationContext;

import javax.sql.DataSource;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;


@ExtendWith(SpringExtension.class)
@ContextConfiguration
@SpringBootTest(classes = UserApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public abstract class IntegrationTestBase {

    @Autowired
    WebApplicationContext context;

    @Autowired
    DataSource dataSource;

    @Value("${local.server.port}")
    private int port;

    @BeforeEach
    public void setUp() throws IOException, SQLException {
        RestAssured.port = port;
        RestAssuredMockMvc.config = RestAssuredMockMvc.config();
        RestAssuredMockMvc.webAppContextSetup(context);
        RunScript.execute(dataSource.getConnection(), new FileReader(new ClassPathResource(
                "sql/test.sql").getFile()));
    }
}
