package com.example.user.integration;

import com.example.user.domain.UserRequest;
import io.restassured.http.ContentType;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import java.time.LocalDate;

import static org.hamcrest.Matchers.equalTo;

class RestITest extends IntegrationTestBase {
    private static final String USERS_PATH = "/rest/users";

    @Test
    void getUsers() {
        RestAssuredMockMvc
                .given()
                .when()
                .get(USERS_PATH)
                .then()
                .log().ifError()
                .statusCode(HttpStatus.OK.value())
                .body("total", equalTo(2))
                .body("data[0].id", equalTo(1))
                .body("data[0].firstName", equalTo("Lokesh"))
                .body("data[0].lastName", equalTo("Gupta"))
                .body("data[0].birthDate", equalTo("2019-10-10"))
                .body("data[0].email", equalTo("howtodoinjava@gmail.com"))
                .body("data[0].address", equalTo("address 1"));
    }

    @Test
    void createUser() {
        RestAssuredMockMvc
                .given()
                .contentType(ContentType.JSON)
                .body(createUserRequest())
                .when()
                .post(USERS_PATH)
                .then()
                .log().ifError()
                .statusCode(HttpStatus.OK.value())
                .body("data", equalTo(3));
        RestAssuredMockMvc
                .given()
                .when()
                .get(USERS_PATH)
                .then()
                .log().ifError()
                .statusCode(HttpStatus.OK.value())
                .body("total", equalTo(3))
                .body("data[2].id", equalTo(3))
                .body("data[2].firstName", equalTo("firstName"))
                .body("data[2].lastName", equalTo("lastName"))
                .body("data[2].birthDate", equalTo("2019-10-10"))
                .body("data[2].email", equalTo("email@email.com"))
                .body("data[2].address", equalTo("address"));
    }

    @Test
    void createUserAlreadyExists() {
        UserRequest userRequest = createUserRequest();
        userRequest.setFirstName("Lokesh");
        userRequest.setLastName("Gupta");
        RestAssuredMockMvc
                .given()
                .contentType(ContentType.JSON)
                .body(userRequest)
                .when()
                .post(USERS_PATH)
                .then()
                .log().all()
                .statusCode(HttpStatus.NOT_ACCEPTABLE.value())
                .body("errorMessage", equalTo("User already exists"));
    }

    @Test
    void updateUser() {
        RestAssuredMockMvc
                .given()
                .contentType(ContentType.JSON)
                .body(createUserRequest())
                .when()
                .put(USERS_PATH + "/{0}", 1)
                .then()
                .log().ifError()
                .statusCode(HttpStatus.OK.value())
                .body("data", equalTo(1));
        RestAssuredMockMvc
                .given()
                .when()
                .get(USERS_PATH)
                .then()
                .log().ifError()
                .statusCode(HttpStatus.OK.value())
                .body("total", equalTo(2))
                .body("data[0].id", equalTo(1))
                .body("data[0].firstName", equalTo("firstName"))
                .body("data[0].lastName", equalTo("lastName"))
                .body("data[0].birthDate", equalTo("2019-10-10"))
                .body("data[0].email", equalTo("email@email.com"))
                .body("data[0].address", equalTo("address"));
    }

    @Test
    void deleteUser() {
        RestAssuredMockMvc
                .given()
                .when()
                .delete(USERS_PATH + "/{0}", 1)
                .then()
                .log().ifError()
                .statusCode(HttpStatus.OK.value());
        RestAssuredMockMvc
                .given()
                .when()
                .get(USERS_PATH)
                .then()
                .log().ifError()
                .statusCode(HttpStatus.OK.value())
                .body("total", equalTo(1))
                .body("data[0].id", equalTo(2))
                .body("data[0].firstName", equalTo("John"))
                .body("data[0].lastName", equalTo("Doe"))
                .body("data[0].birthDate", equalTo("2017-01-25"))
                .body("data[0].email", equalTo("xyz@email.com"))
                .body("data[0].address", equalTo("address 2"));
    }

    private UserRequest createUserRequest() {
        return UserRequest.builder()
                .firstName("firstName")
                .lastName("lastName")
                .birthDate(LocalDate.of(2019, 10, 10))
                .email("email@email.com")
                .address("address")
                .build();
    }


}
