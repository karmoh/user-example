package com.example.user.service;

import com.example.user.domain.UserRequest;
import com.example.user.entity.UserEntity;
import com.example.user.exeption.BusinessException;
import com.example.user.exeption.ResourceNotFoundException;
import com.example.user.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @Captor
    private ArgumentCaptor<UserEntity> userEntityArgumentCaptor;
    @Mock
    private UserEntity userEntity;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getUsers() {
        when(userRepository.findAll()).thenReturn(Collections.singletonList(UserEntity.builder().id(1L).build()));
        List<UserEntity> result = crateService().getUsers();
        assertFalse(result.isEmpty());
    }

    @Test
    void getUserNames() {
        UserEntity userEntity = UserEntity.builder()
                .firstName("firstName")
                .lastName("lastName")
                .build();
        when(userRepository.findAll()).thenReturn(Collections.singletonList(userEntity));
        List<String> result = crateService().getUserNames();
        assertEquals("firstName lastName", result.get(0));
    }

    @Test
    void createUserEmailValidation() {
        UserRequest userRequest = createUserRequest();
        Arrays.asList("email@example.com",
                "firstname.lastname@example.com",
                "email@subdomain.example.com",
                "firstname+lastname@example.com",
                "email@[123.123.123.123]",
                "\"email\"@example.com",
                "1234567890@example.com",
                "email@example-one.com",
                "_______@example.com",
                "email@example.name",
                "email@example.museum",
                "email@example.co.jp",
                "firstname-lastname@example.com").forEach(email -> {
            userRequest.setEmail(email);
            crateService().createUser(userRequest);

        });
        verify(userRepository, times(13)).save(any(UserEntity.class));
        Arrays.asList("plainaddress",
                "#@%^%#$@#$@#.com",
                "@example.com",
                "Joe Smith <email@example.com>",
                "email.example.com",
                "email@example@example.com",
                ".email@example.com",
                "email.@example.com",
                "email..email@example.com",
                "email@example.com (Joe Smith)",
                "email@example",
                "email@-example.com",
                "email@example.web",
                "email@111.222.333.44444",
                "email@example..com",
                "Abc..123@example.com").forEach(email -> {
            userRequest.setEmail(email);
            BusinessException thrown =
                    assertThrows(BusinessException.class,
                            () -> crateService().createUser(userRequest));

            assertEquals("Email " + email + " is not valid", thrown.getMessageText());
        });
    }

    @Test
    void createUser() {
        UserRequest userRequest = createUserRequest();
        crateService().createUser(userRequest);
        verify(userRepository).save(userEntityArgumentCaptor.capture());
        UserEntity userEntity = userEntityArgumentCaptor.getValue();
        assertEquals(userRequest.getFirstName(), userEntity.getFirstName());
        assertEquals(userRequest.getLastName(), userEntity.getLastName());
        assertEquals(userRequest.getBirthDate(), userEntity.getBirthDate());
        assertEquals(userRequest.getEmail(), userEntity.getEmail());
        assertEquals(userRequest.getAddress(), userEntity.getAddress());
    }

    @Test
    void createUserFieldValidation() {
        UserRequest userRequest = createUserRequest();
        userRequest.setAddress(null);
        BusinessException thrown =
                assertThrows(BusinessException.class,
                        () -> crateService().createUser(userRequest));
        assertEquals("Field address is mandatory", thrown.getMessageText());
        userRequest.setBirthDate(null);
        thrown =
                assertThrows(BusinessException.class,
                        () -> crateService().createUser(userRequest));
        assertEquals("Fields address, birth date are mandatory", thrown.getMessageText());
    }

    @Test
    void createUserUserAlreadyExists() {

        UserRequest userRequest = createUserRequest();
        when(userRepository.findUserByName(userRequest.getFirstName(),
                userRequest.getLastName())).thenReturn(Collections.singletonList(userEntity));
        BusinessException thrown =
                assertThrows(BusinessException.class,
                        () -> crateService().createUser(userRequest));
        assertEquals("User already exists", thrown.getMessageText());
    }

    @Test
    void updateUserUserAlreadyExists() {
        when(userRepository.findById(1L)).thenReturn(Optional.of(userEntity));
        when(userEntity.getId()).thenReturn(1L);
        UserRequest userRequest = createUserRequest();
        when(userRepository.findUserByName(userRequest.getFirstName(),
                userRequest.getLastName())).thenReturn(Collections.singletonList(userEntity));
        when(userEntity.getId()).thenReturn(2L);

        BusinessException thrown =
                assertThrows(BusinessException.class,
                        () -> crateService().updateUser(1L, userRequest));
        assertEquals("User already exists", thrown.getMessageText());
    }

    @Test
    void updateUser() {
        when(userRepository.findById(1L)).thenReturn(Optional.of(userEntity));
        when(userEntity.getId()).thenReturn(1L);
        UserRequest userRequest = createUserRequest();
        when(userRepository.findUserByName(userRequest.getFirstName(),
                userRequest.getLastName())).thenReturn(Collections.singletonList(userEntity));
        when(userEntity.getId()).thenReturn(1L);
        crateService().updateUser(1L, userRequest);
        verify(userRepository).save(userEntityArgumentCaptor.capture());
        UserEntity userEntity = userEntityArgumentCaptor.getValue();
        assertEquals(userRequest.getFirstName(), userEntity.getFirstName());
        assertEquals(userRequest.getLastName(), userEntity.getLastName());
        assertEquals(userRequest.getBirthDate(), userEntity.getBirthDate());
        assertEquals(userRequest.getEmail(), userEntity.getEmail());
        assertEquals(userRequest.getAddress(), userEntity.getAddress());
        assertEquals(1L, userEntity.getId().longValue());
    }

    @Test
    void deleteUser() {
        when(userRepository.findById(1L)).thenReturn(Optional.of(userEntity));
        when(userEntity.getId()).thenReturn(1L);
        crateService().deleteUser(1L);
        verify(userRepository).deleteById(1L);
    }

    @Test
    void getUserById() {
        when(userRepository.findById(1L)).thenReturn(Optional.empty());

        ResourceNotFoundException thrown =
                assertThrows(ResourceNotFoundException.class,
                        () -> crateService().getUserById(1L));
        assertEquals("User not found with id 1", thrown.getMessageText());
    }

    @Test
    void getUserByIdNotExists() {
        when(userRepository.findById(1L)).thenReturn(Optional.of(userEntity));
        UserEntity result = crateService().getUserById(1L);
        assertNotNull(result);
    }

    private UserRequest createUserRequest() {
        return UserRequest.builder()
                .firstName("firstName")
                .lastName("lastName")
                .birthDate(LocalDate.of(2019, 10, 10))
                .email("email@email.com")
                .address("address")
                .build();
    }

    private UserService crateService() {
        return new UserService(userRepository);
    }
}