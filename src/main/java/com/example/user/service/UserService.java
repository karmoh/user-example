package com.example.user.service;

import com.example.user.domain.UserRequest;
import com.example.user.entity.UserEntity;
import com.example.user.exeption.BusinessException;
import com.example.user.exeption.ResourceNotFoundException;
import com.example.user.repository.UserRepository;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<UserEntity> getUsers() {
        return userRepository.findAll();
    }

    public List<String> getUserNames() {
        return userRepository.findAll().stream().map(userEntity -> userEntity.getFirstName() + " " + userEntity.getLastName()).collect(Collectors.toList());
    }

    public Long createUser(UserRequest request) {
        validateUserRequest(request);
        validateUserName(request.getFirstName(), request.getLastName(), null);
        UserEntity userEntity = mapUserRequestToUserData(null, request);
        userRepository.save(userEntity);
        return userEntity.getId();
    }

    private void validateUserName(String firstName, String lastName, Long ignoreId) {
        List<UserEntity> users = userRepository.findUserByName(firstName, lastName);
        if (CollectionUtils.isEmpty(users) || (users.size() == 1 && ignoreId != null && ignoreId.equals(users.get(0).getId()))) {
            return;
        }
        throw new BusinessException("User already exists");
    }

    private void validateUserRequest(UserRequest request) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<UserRequest>> violations = validator.validate(request);
        if (CollectionUtils.isEmpty(violations)) {
            return;
        }
        List<String> fields = new ArrayList<>();
        for (ConstraintViolation<UserRequest> violation : violations) {
            fields.add(splitCamelCaseString(violation.getPropertyPath().toString()));
        }
        fields.sort(Comparator.naturalOrder());
        String message;
        if (fields.size() == 1) {
            message = "Field " + fields.get(0) + " is mandatory";
        } else {
            message = "Fields " + String.join(", ", fields) + " are mandatory";
        }
        throw new BusinessException(message);
    }

    private static String splitCamelCaseString(String s) {
        List<String> result = new ArrayList<>();
        for (String w : s.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])")) {
            result.add(w.toLowerCase());
        }
        return String.join(" ", result);
    }

    public Long updateUser(Long userId, UserRequest request) {
        validateUserRequest(request);
        UserEntity userEntity = getUserById(userId);
        validateUserName(request.getFirstName(), request.getLastName(), userId);
        userRepository.save(mapUserRequestToUserData(userEntity.getId(), request));
        return userEntity.getId();
    }

    public void deleteUser(Long userId) {
        UserEntity userEntity = getUserById(userId);
        userRepository.deleteById(userEntity.getId());
    }

    public UserEntity getUserById(Long userId) {
        UserEntity userEntity = userRepository.findById(userId).orElse(null);
        if (userEntity == null) {
            throw new ResourceNotFoundException("User not found with id " + userId);
        }
        return userEntity;
    }

    private UserEntity mapUserRequestToUserData(Long userId, UserRequest request) {
        UserEntity userEntity = UserEntity.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .birthDate(request.getBirthDate())
                .email(validateEmail(request.getEmail()))
                .address(request.getAddress())
                .build();
        if (userId != null) {
            userEntity.setId(userId);
        }
        return userEntity;
    }

    private String validateEmail(String email) {
        EmailValidator eValidator = EmailValidator.getInstance();
        if (!eValidator.isValid(email)) {
            throw new BusinessException("Email " + email + " is not valid");
        }
        return email;
    }
}
