package com.example.user.web;

import com.example.user.domain.ServiceResponse;
import com.example.user.domain.UserRequest;
import com.example.user.entity.UserEntity;
import com.example.user.exeption.BusinessException;
import com.example.user.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
public class UserRestController {

    private final UserService userService;

    public UserRestController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping("/rest/user-names")
    private List<String> getUserNames() {
        return userService.getUserNames();
    }

    @GetMapping("/rest/users")
    public ServiceResponse<List<UserEntity>> getUsers() {
        List<UserEntity> result = userService.getUsers();
        return ServiceResponse.ok(result, result.size());
    }

    @PostMapping("/rest/users")
    public ServiceResponse<Long> createUser(@RequestBody @Valid UserRequest user
    ) {
        return ServiceResponse.ok(userService.createUser(user));
    }

    @PutMapping("/rest/users/{userId}")
    public ServiceResponse<Long> updateUser(@PathVariable Long userId,
                                            @RequestBody @Valid UserRequest user
    ) {
        return ServiceResponse.ok(userService.updateUser(userId, user));
    }

    @DeleteMapping("/rest/users/{userId}")
    public ServiceResponse deleteUser(
            @PathVariable Long userId
    ) {
        userService.deleteUser(userId);
        return ServiceResponse.ok();
    }
}
