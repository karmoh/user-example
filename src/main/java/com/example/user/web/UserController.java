package com.example.user.web;

import com.example.user.domain.UserRequest;
import com.example.user.entity.UserEntity;
import com.example.user.exeption.BusinessException;
import com.example.user.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

@Controller("/app")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String getUsers(Model model) {
        model.addAttribute("users", userService.getUsers());
        return "list-users";
    }

    @GetMapping(path = {"/create", "/edit/{id}"})
    public String editUserById(Model model, @PathVariable("id") Optional<Long> id) {
        if (id.isPresent()) {
            UserEntity entity = userService.getUserById(id.get());
            model.addAttribute("user", entity);
        } else {
            model.addAttribute("user", new UserEntity());
        }
        model.addAttribute("error", "no");
        return "add-edit-user";
    }

    @RequestMapping(value = "/create-user")
    public String createUser(Model model, UserRequest user
    ) {
        try {
            userService.createUser(user);
            return "redirect:/";
        } catch (BusinessException e) {
            model.addAttribute("user", UserEntity.builder()
                    .firstName(user.getFirstName())
                    .lastName(user.getLastName())
                    .birthDate(user.getBirthDate())
                    .email(user.getEmail())
                    .address(user.getAddress())
                    .build());
            model.addAttribute("error", e.getMessageText());
            return "add-edit-user";
        }
    }

    @RequestMapping("/update-user/{userId}")
    public String updateUser(Model model,
                             @PathVariable Long userId,
                             UserRequest user
    ) {

        try {
            userService.updateUser(userId, user);
            return "redirect:/";
        } catch (BusinessException e) {
            model.addAttribute("user", UserEntity.builder()
                    .id(userId)
                    .firstName(user.getFirstName())
                    .lastName(user.getLastName())
                    .birthDate(user.getBirthDate())
                    .email(user.getEmail())
                    .address(user.getAddress())
                    .build());
            model.addAttribute("error", e.getMessageText());
            return "add-edit-user";
        }
    }

    @DeleteMapping("delete-user/{userId}")
    public String deleteUser(
            @PathVariable Long userId
    ) {
        userService.deleteUser(userId);
        return "redirect:/";
    }
}
