package com.example.user.exeption;

public class ResourceNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -8375655612540699875L;
    private final String messageText;

    public ResourceNotFoundException(String messageText) {
        this.messageText = messageText;
    }

    public String getMessageText() {
        return messageText;
    }
}
