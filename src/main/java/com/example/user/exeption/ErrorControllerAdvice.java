package com.example.user.exeption;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.invoke.MethodHandles;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@ControllerAdvice
@RequestMapping(produces = "application/json")
public class ErrorControllerAdvice {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ErrorMessage> notFoundException(final ResourceNotFoundException e) {
        ErrorMessage responseMessage = new ErrorMessage(UUID.randomUUID().toString(), e.getMessageText(), LocalDateTime.now(Clock.systemUTC()));
        return new ResponseEntity<>(responseMessage, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(BindException.class)
    public ResponseEntity<ErrorMessage> notFoundException(final BindException e) {
        List<String> fields = new ArrayList<>();
        e.getFieldErrors().forEach(fieldError -> {
            fields.add(splitCamelCaseString(fieldError.getField()));
        });
        String message;
        if (fields.size() == 1) {
            message = "Field " + fields.get(0) + " is mandatory";
        } else {
            message = "Fields " + String.join(", ", fields) + " are mandatory";
        }
        ErrorMessage responseMessage = new ErrorMessage(UUID.randomUUID().toString(), message, LocalDateTime.now(Clock.systemUTC()));
        return new ResponseEntity<>(responseMessage, HttpStatus.NOT_ACCEPTABLE);
    }

    public static String splitCamelCaseString(String s) {
        List<String> result = new ArrayList<>();
        for (String w : s.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])")) {
            result.add(w.toLowerCase());
        }
        return String.join(" ", result);
    }

    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<ErrorMessage> businessException(final BusinessException e) {
        ErrorMessage responseMessage = new ErrorMessage(UUID.randomUUID().toString(), e.getMessageText(), LocalDateTime.now(Clock.systemUTC()));
        return new ResponseEntity<>(responseMessage, HttpStatus.NOT_ACCEPTABLE);
    }

    @ExceptionHandler({TechnicalException.class, Exception.class})
    public ResponseEntity<ErrorMessage> technicalException(final Exception exception) {
        String errorId = UUID.randomUUID().toString();
        ErrorMessage responseMessage = new ErrorMessage(errorId, "Technical exception ref:" + errorId, LocalDateTime.now(Clock.systemUTC()));
        LOGGER.error(responseMessage.toString(), exception);
        return new ResponseEntity<>(responseMessage, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
