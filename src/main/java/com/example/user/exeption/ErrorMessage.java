package com.example.user.exeption;


import lombok.*;

import java.time.LocalDateTime;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ErrorMessage {
    private String ref;
    private String errorMessage;
    private LocalDateTime dateTime;

}
